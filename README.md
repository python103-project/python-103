# Airport Construction 
- We model the Boundary of Piconesia as a polygon.
- Given the polygon we need to compute the length of the longest landing strip.
- The landing strip must not intersect the sea.
- but it may touch or run along the boundary of the island.

# Team Members
- Sahithi: 19WH1A0555 : CSE
- Sri Sindhu: 19WH1A0547 : CSE
- Y Pravalika: 19WH1A12A6 : IT 
- K Nishitha: 19WH1A1288 : IT
- M Shruthi: 19WH1A0421 : ECE
- T Sirisha: 19WH1A0443 : ECE
- S Malavika: 20WH5A0517 : CSE
